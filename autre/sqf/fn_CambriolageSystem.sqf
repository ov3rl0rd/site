#include "..\..\script_macros.hpp"
/*
    Author: ZACHARY RUC

    Description : burglary of differencies houses with an object in the houses
*/
params [
    ["_shop",ObjNull],
    ["_robber",ObjNull],
    ["_action",-1]
];

/*========Configuration=========*/
_number_cop = 1; //Nomber of cop for can burglary an houses
_add_interpol = false; //add or not the author of crime in interpol
_marker_create = true; //active or not the creation of marker
_money_fix = 1000; //fixed money earned
_blocked_time = 1350; // Time before the next burglary
_money_random = 4000; //random money in addition to fixed money
_time = 150; //time for the burglary in seconds
_temps_atm = 30; //Time before the robber can deposit his money into the ATM (in minutes)
/*=============================*/

if (side _robber != civilian) exitWith {hint "Tu dois etre un civil !"}; //You must be a civilian!
if ((west countSide playableUnits) < _number_cop) exitWith {hint "Il n'y a pas assez de Gendarmes !"}; //There are not enough cops!
if (_robber distance _shop > 5) exitWith {hint "Tu dois etre à 5m !"}; //You must be 5m away!
if (vehicle player != _robber) exitWith {hint "Tu dois d'abord sortir de ton véhicule !"}; //First you have to get out of your vehicle!
if !(alive _robber) exitWith {};
if (life_inv_pied_de_biche isEqualTo 0) exitWith {hint "Tu n'as pas l'objet requis pied de biche pour cambriolé!";}; //You don't have the required crowbar item to rob!
if (_shop getVariable ["robbery_progress",false]) exitWith {hint "Il y a déjà un cambriolage en cours"}; //There is already a burglary underway

_exit = false;
_money = _money_fix + round (random _money_random);
_title = _shop actionParams _action;
_shop removeAction _action;
_shop setVariable ["robbery_progress",true,true];

_fn_create_Marker = {
    _marker = createMarker [format ["marker_var_%1",_action], _shop];
    _marker setMarkerColor "ColorRed";
    _marker setMarkerText "ATTENTION: CAMBRIOLAGE EN COURS !"; //WARNING: BURGLARY IN PROGRESS!
    _marker setMarkerType "mil_warning";
};

if ((random 100) >= 50) then {
    [1,"ALERTE CAMBRIOLAGE : Une alarme vient d’être déclenchée dans une maison !"] remoteExec ["life_fnc_broadcast",west]; //BURGLARY ALERT: An alarm has just been triggered in a house!
    [] call _fn_create_Marker;
};

disableSerialization;

"progressBar" cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable "life_progress";
_progress = _ui displayCtrl 38201;
_pgText = _ui displayCtrl 38202;
_pgText ctrlSetText "Cambriolage en cours restez proche ! (5m) (1%)..."; //Burglary in progress stay close! (5m) (1%)...
_progress progressSetPosition 0.01;
_cP = 0.01;

if (_marker_create) then {
    [] call _fn_create_Marker;
};

for "_i" from 0 to 1 step 0 do {
    uiSleep (_time / 100);

    _cP = _cP + 0.01;
    _progress progressSetPosition _cP;
    _pgText ctrlSetText format ["Cambriolage en cours rester proche ! (5m)  (%1%2)...",round (_cP * 100), "%"]; //Burglary in progress stay close! (5m)   (%1%2)...",round (_cP * 100), "%"

    if (_cP >= 1) exitWith {};
    if (_robber distance _shop > 5) exitWith {_exit = true};
    if (!alive _robber) exitWith {_exit = true};
    if (_robber getVariable "restrained") exitWith {_exit = true};
    if (life_istazed) exitWith {_exit = true};                                  
};

if (_exit) exitWith {
    "progressBar" cutText ["","PLAIN"];
    hint "La maison a été fermée et la gendarmerie a été prévenu !"; //The house was closed and the cops was notified!
    [1,format["Une personne vient d'essayer de cambriolé une maison !",_robber,name _robber, _shop]] remoteExec ["life_fnc_broadcast",west]; //A person just tried to rob a house!

    playSound3D ["A3\Sounds_F\sfx\alarm_independent.wss", _robber];
    deleteMarker format ["marker_var_%1",_action];
    _shop setVariable ["robbery_progress",false,true];

    if (_add_interpol) then {
        [getPlayerUID _robber, _robber getVariable ["realname", name _robber], "211"] remoteExecCall ["life_fnc_wantedAdd", RSERV];
    };
};

"progressBar" cutText ["","PLAIN"];
titleText[format["Vous avez volé %1, maintenant fuyez avant que le gendarmerie arrive !",[_money] call life_fnc_numberText], "PLAIN"]; //You stole %1, now flee before the cops arrives!
playSound3D ["A3\Sounds_F\sfx\alarm_independent.wss", _robber];
[1, format["News : Une maison d’être cambriolé de %1 €",[_money] call life_fnc_numberText]] remoteExec ["life_fnc_broadcast", west]; //News: A house to be robbed of %1 €

if (_add_interpol) then {
    [getPlayerUID _robber, _robber getVariable ["realname",name _robber], "211"] remoteExecCall ["life_fnc_wantedAdd", RSERV];
};

life_cash = life_cash + _money;
call SOCK_fnc_updatePartial;
deleteMarker format ["marker_var_%1",_action];
_shop setVariable ["robbery_progress",false,true];

[_shop,_title,_blocked_time] spawn {
    sleep (_this select 2);
    (_this select 0) addAction [((_this select 1) select 0),life_fnc_robSystem];
};

sleep (_temps_atm * 60);
life_use_atm = true;