/*
    File: fn_gazer.sqf
    Author: Zachary RUC alias Ov3rl0rd

    Description:
    Quand on se fait gaser
*/


closeDialog 0;

"radialBlur" ppEffectEnable true;
_blurred = ppEffectCreate ["DynamicBlur", 500];
//10 secondes d'effets
for "_i" from 0 to 10 do
{
    _blurred ppeffectenable true;
    _blurred ppeffectadjust [15];
    _blurred ppeffectcommit 1;
    "radialBlur" ppEffectAdjust  [random 0.02,random 0.02,0.15,0.15];
    "radialBlur" ppEffectCommit 1;  
    addcamShake[random 3, 1, random 3];
    sleep 1;
};

//Stop effects
_blurred ppEffectAdjust [0];
_blurred ppEffectCommit 5;
"radialBlur" ppEffectAdjust  [0,0,0,0];
"radialBlur" ppEffectCommit 5;
sleep 6;

//Stop tous
_blurred ppEffectEnable false;
"radialBlur" ppEffectEnable false;
resetCamShake;